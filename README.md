# getting started

1. fork git repository
1. git clone your fork
1. install npm packages `npm install`
1. testing:
   - unit ([jest](https://jestjs.io/)): `npm run test`
   - lint ([eslint](https://eslint.org/)): `npm run lint`
1. create merge request from your fork

# tasks

> * Check [examples.js](./examples.js) for code usage examples
> * Tasks can be made in any order

 * update code style rules in this project:
    - turned of `linebreak-style` rule
    - use `'` instead of `"`
    - use 2 spaces for indent
    - remove any usage of var (eslint `no-var`)
 * fix linting errors and do not make new errors in future
 * add git hook to run linter locally each time you commit new code (you can use [husky](https://www.npmjs.com/package/husky) or any other tool you like)
 * add ci job to run linter
 * test `datestring.js` with mocking `time.js`
    - check from mock that `ms` params set to seconds
    - use few differnet [date formats](https://momentjs.com/docs/#/displaying/format/)
 * use snapshot testing to test `company.js` (use ids 3, 85)
 * use string as input(id) for `company.js` - test that exception is given
 * add 2 more test cases to `toNumber.test.js`
 * write implementation (`lowercase.js`) for this test
```js
// src/lowercase.spec.js
const lowercase = require('./lowercase');

describe('lowercase', () => {
  it('CAT => cat', () => {
    expect(lowercase('CAT')).toBe("cat");
  });
  it('error - bad input', () => {
    expect(() => {
      capitalize({ "word": 'cat' });
    }).toThrow(/bad input/);
  });
});
```
 * make one random improvements
    - **NB:** make it with separate commit so this can be revied
